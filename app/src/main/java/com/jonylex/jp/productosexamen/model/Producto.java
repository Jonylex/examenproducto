package com.jonylex.jp.productosexamen.model;

import java.util.Date;

public class Producto {

    private int _productos_id_producto;
    private String _productos_serie;
    private String _productos_nombre_producto;
    private String _productos_descripción;
    private Double _productos_precio;
    private String _productos_urlImg;
    private String  _productos_fecha_alta;
    private Double _productos_lng_producto;
    private Double _productos_lat_producto;
    private String _productos_usuario_alta;
    private String _productos_async;

    public Producto(int _productos_id_producto, String _productos_serie, String _productos_nombre_producto, String _productos_descripción, Double _productos_precio, String _productos_urlImg, String _productos_fecha_alta, Double _productos_lng_producto, Double _productos_lat_producto, String _productos_usuario_alta, String _productos_async) {
        this._productos_id_producto = _productos_id_producto;
        this._productos_serie = _productos_serie;
        this._productos_nombre_producto = _productos_nombre_producto;
        this._productos_descripción = _productos_descripción;
        this._productos_precio = _productos_precio;
        this._productos_urlImg = _productos_urlImg;
        this._productos_fecha_alta = _productos_fecha_alta;
        this._productos_lng_producto = _productos_lng_producto;
        this._productos_lat_producto = _productos_lat_producto;
        this._productos_usuario_alta = _productos_usuario_alta;
        this._productos_async = _productos_async;
    }

    public Producto(){}

    public int get_productos_id_producto() {
        return _productos_id_producto;
    }

    public void set_productos_id_producto(int _productos_id_producto) {
        this._productos_id_producto = _productos_id_producto;
    }

    public String get_productos_serie() {
        return _productos_serie;
    }

    public void set_productos_serie(String _productos_serie) {
        this._productos_serie = _productos_serie;
    }

    public String get_productos_nombre_producto() {
        return _productos_nombre_producto;
    }

    public void set_productos_nombre_producto(String _productos_nombre_producto) {
        this._productos_nombre_producto = _productos_nombre_producto;
    }

    public String get_productos_descripción() {
        return _productos_descripción;
    }

    public void set_productos_descripción(String _productos_descripción) {
        this._productos_descripción = _productos_descripción;
    }

    public Double get_productos_precio() {
        return _productos_precio;
    }

    public void set_productos_precio(Double _productos_precio) {
        this._productos_precio = _productos_precio;
    }

    public String get_productos_urlImg() {
        return _productos_urlImg;
    }

    public void set_productos_urlImg(String _productos_urlImg) {
        this._productos_urlImg = _productos_urlImg;
    }

    public String get_productos_fecha_alta() {
        return _productos_fecha_alta;
    }

    public void set_productos_fecha_alta(String _productos_fecha_alta) {
        this._productos_fecha_alta = _productos_fecha_alta;
    }

    public Double get_productos_lng_producto() {
        return _productos_lng_producto;
    }

    public void set_productos_lng_producto(Double _productos_lng_producto) {
        this._productos_lng_producto = _productos_lng_producto;
    }

    public Double get_productos_lat_producto() {
        return _productos_lat_producto;
    }

    public void set_productos_lat_producto(Double _productos_lat_producto) {
        this._productos_lat_producto = _productos_lat_producto;
    }

    public String get_productos_usuario_alta() {
        return _productos_usuario_alta;
    }

    public void set_productos_usuario_alta(String _productos_usuario_alta) {
        this._productos_usuario_alta = _productos_usuario_alta;
    }

    public String get_productos_async() {
        return _productos_async;
    }

    public void set_productos_async(String _productos_async) {
        this._productos_async = _productos_async;
    }



}
