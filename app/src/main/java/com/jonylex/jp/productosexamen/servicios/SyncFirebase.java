package com.jonylex.jp.productosexamen.servicios;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jonylex.jp.productosexamen.R;
import com.jonylex.jp.productosexamen.model.Producto;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDContract;
import com.jonylex.jp.productosexamen.ui.MainActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class SyncFirebase extends Service {

    Handler mHandler=new Handler();

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            runa();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void runa() throws Exception{
        final Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                sincronizaFirebase();
                mHandler.post(new Runnable(){
                                  public void run(){
                                      Toast.makeText(SyncFirebase.this, "Actualizando", Toast.LENGTH_LONG).show();
                                  }
                              });


            }
        }, 1000, 300000);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    private void sincronizaFirebase() {
        try {
            FirebaseAuth mAuth;
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            DatabaseReference usersRef = ref.child("productos");
            mAuth = FirebaseAuth.getInstance();
            FirebaseUser userFacebook = mAuth.getCurrentUser();
            final ContentResolver _resolver;
            _resolver = getContentResolver();
            Map<String, Producto> productos = new HashMap<>();
            Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.getURIProductosFilter("async"), null, null, null, null);
            DatabaseUtils.dumpCursor(_c);
            if(_c.getCount() > 0 ){
                _c.moveToPosition(0);
                do {
                    productos.put(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)),
                            new Producto(
                                    Integer.parseInt(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.ID_PRODUCTO))),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO)),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO)),
                                    Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.PRECIO_PRODUCTO))),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.URLIMG_PRODUCTO)),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO)),
                                    Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LNG_PRODUCTO))),
                                    Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LAT_PRODUCTO))),
                                    userFacebook.getDisplayName(),
                                    "1"));

                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReference();
                    Uri file = Uri.fromFile(new File(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.URLIMG_PRODUCTO))));
                    StorageReference riversRef = storageRef.child("images/" + _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)));
                    UploadTask uploadTask = riversRef.putFile(file);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        }
                    });

                } while (_c.moveToNext());
                _c.close();

                usersRef.push().setValue(productos).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(@NonNull Void T) {

                        Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.getURIProductosFilter("async"), null, null, null, null);
                        DatabaseUtils.dumpCursor(_c);
                        _c.moveToPosition(0);
                        do {

                            ArrayList<ContentProviderOperation> _ops = new ArrayList<>();
                            _ops.add(ContentProviderOperation.newUpdate(ProductosBDContract.productos_articulos.getURIProductos(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.ID_PRODUCTO))))
                                    .withValue(ProductosBDContract.productos_articulos.ASYNC_PRODUCTO, "1")
                                    .build());
                            try {
                                _resolver.applyBatch(ProductosBDContract.AUTHORITY_PRODUCTOS, _ops);


                            } catch (RemoteException e) {
                                e.printStackTrace();

                            } catch (OperationApplicationException e) {
                                e.printStackTrace();
                            }


                        } while (_c.moveToNext());
                        _c.close();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }

        }
        catch (Exception e){
            e.printStackTrace();

        }
    }
}
