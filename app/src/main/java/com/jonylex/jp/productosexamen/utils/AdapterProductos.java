package com.jonylex.jp.productosexamen.utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.jonylex.jp.productosexamen.R;
import com.jonylex.jp.productosexamen.model.Producto;
import com.jonylex.jp.productosexamen.ui.DetallesProducto;
import java.util.ArrayList;
import java.util.List;

public class AdapterProductos extends RecyclerView.Adapter<AdapterProductos.ViewHolder> implements Filterable {

    private List<Producto> list;
    private List<Producto> productosFull;


    public AdapterProductos(List<Producto> list){

        this.list = list;
        productosFull = new ArrayList<>(list);

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_productos, parent, false));


         }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Producto data = list.get(position);
        holder.txtNombre.setText(data.get_productos_nombre_producto());
        holder.txtDesc.setText(data.get_productos_descripción());
        holder.txtPrecio.setText(data.get_productos_precio().toString());

        final Intent intent = new Intent(holder.context, DetallesProducto.class);
        intent.putExtra("codigo", data.get_productos_serie());


        Glide.with(holder.context)
                .load(data.get_productos_urlImg().toString())
                .into( holder.ivImgProductos);

        holder.txtNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              holder.context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });

        holder.txtDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });


        holder.txtPrecio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });

        holder.ivImgProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });
/*
        Glide.with(holder.context)
                .asBitmap()
                .load(data.get_productos_urlImg().toString())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        holder.ivImgProductos.setImageBitmap(resource);
                    }

                });*/

        //Bitmap bitmap = cargaConGlide(holder.context, data.get_productos_precio().toString());  //galleryAddPic(data.get_productos_urlImg(), holder.ivImgProductos, holder.context);



        //if (bitmap != null)
          //  holder.ivImgProductos.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return productosFilter;
    }

    private Filter productosFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Producto> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(productosFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Producto item : productosFull) {
                    if (item.get_productos_nombre_producto().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list.clear();
            list.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class  ViewHolder extends RecyclerView.ViewHolder{
        TextView txtNombre;
        TextView txtDesc;
        TextView txtPrecio;
        ImageView ivImgProductos;
        Context context;

        public ViewHolder(View itemView) {
            super(itemView);

            txtNombre = itemView.findViewById(R.id.txtNombreItem);
            txtDesc = itemView.findViewById(R.id.txtDescItem);
            txtPrecio = itemView.findViewById(R.id.txtPrecioItem);
            ivImgProductos = itemView.findViewById(R.id.ivImgProducto);
            context =   itemView.getContext();


        }
    }
}
