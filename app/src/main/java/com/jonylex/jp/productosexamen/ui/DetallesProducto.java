package com.jonylex.jp.productosexamen.ui;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jonylex.jp.productosexamen.R;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDContract;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDInstance;

import java.util.ArrayList;
import java.util.Calendar;


public class DetallesProducto extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnMapReadyCallback{

    private static final String TAG = DetallesProducto.class.getName();
    ContentResolver _resolver;
    private RelativeLayout mBottomSheet;
    BottomSheetBehavior bsb;
    private GoogleMap mMap;
    ArrayList<LatLng> MarkerPoints;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    MapView mapView;
    Toolbar toolbar;

    TextView txtNombreDetalle;
    //TextView txtSerialDetalle;
    TextView txtDescDetalle;
    TextView txtPrecioDetalle;
    ImageView ivProductoDetalle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_producto);
        _resolver = getContentResolver();

        mBottomSheet = (RelativeLayout) findViewById(R.id.bottomSheet_choose);
        bsb = BottomSheetBehavior.from(mBottomSheet);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mapView = (MapView) findViewById(R.id.map);

         txtNombreDetalle = (TextView) findViewById(R.id.txtNombreDetalles);
        txtDescDetalle = (TextView) findViewById(R.id.txtDescDetalles);
        txtPrecioDetalle = (TextView) findViewById(R.id.txtPrecioDetalle);
      //  txtSerialDetalle = (TextView) findViewById(R.id.txtSerialDetalles);
         ivProductoDetalle = (ImageView) findViewById(R.id.ivProductoDetalle);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detalle el producto");


        if (mapView != null) {
            MarkerPoints = new ArrayList<>();
            // Initialise the MapView
            mapView.onCreate(null);
            mapView.onResume();

            // Set the map ready callback to receive the GoogleMap object
            mapView.getMapAsync(this);

        }




    }

    private void getProducto(String codigo) {
        _resolver = getContentResolver();
        Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.getURIProductos(codigo), null, null, null, null);
        if (_c.getCount() > 0){
            _c.moveToPosition(0);
            txtNombreDetalle.setText(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO)));
            txtDescDetalle .setText(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO)));
            txtPrecioDetalle.setText("$ " + Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.PRECIO_PRODUCTO))));
        //    txtSerialDetalle .setText(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)));


            String imgProducto = _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.URLIMG_PRODUCTO));

            Glide.with(DetallesProducto.this).load(imgProducto).asBitmap().centerCrop().into(new BitmapImageViewTarget(ivProductoDetalle) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivProductoDetalle.setImageDrawable(circularBitmapDrawable);
                }
            });

            Double lng = Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LNG_PRODUCTO)));
            Double lat = Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LAT_PRODUCTO)));

            LatLng latLng = new LatLng(lat, lng);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Toque aquí para más información...");

            mCurrLocationMarker = mMap.addMarker(markerOptions);

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
            mMap.animateCamera(cameraUpdate);


        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        ProductosBDInstance.getInstancia(this);


    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker arg0) {
                bsb.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);

            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
        }
        MapStyleOptions style;

        style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_day);
        boolean success = mMap.setMapStyle(style);
        if (!success) {
            Log.e(TAG, "Style parsing failed.");

        }
        if(getIntent().getExtras() != null)
            getProducto( getIntent().getExtras().getString("codigo"));

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();

    }



}
