package com.jonylex.jp.productosexamen.sqlite;

import android.content.Context;

public class ProductosBDInstance {
    public static ProductosBDHelper objBD;
    private static ProductosBDInstance instancia = new ProductosBDInstance();

    private ProductosBDInstance(){
    }

    public static ProductosBDInstance getInstancia(Context _myContext){
        if(objBD == null){
            objBD = new ProductosBDHelper(_myContext);
        }
        return instancia;
    }
}
