package com.jonylex.jp.productosexamen.utils;

public class Constantes {
    /**
     * DATABASE NAME
     * */
    public static final String DATABASE_NAME = "tappxi_db.db";

    /**
     * DATABASE VERSION...
     * */
    public static final int DATABASE_VERSION = 1;

    /**
     * URL DE FIREBASE...
     * */
    public static final String URL_FIREBASE = "gs://tappxibeta.appspot.com";

}
