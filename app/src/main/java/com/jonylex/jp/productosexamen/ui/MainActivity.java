package com.jonylex.jp.productosexamen.ui;


import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.Result;
import com.jonylex.jp.productosexamen.R;
import com.jonylex.jp.productosexamen.controller.ProductosController;
import com.jonylex.jp.productosexamen.model.Producto;
import com.jonylex.jp.productosexamen.servicios.SyncFirebase;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDContract;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDInstance;
import com.jonylex.jp.productosexamen.utils.AdapterProductos;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import static android.Manifest.permission.CAMERA;


public class MainActivity extends AppCompatActivity implements  ZXingScannerView.ResultHandler{

    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_NUEVO_ARTICULO = 2 ;
    RecyclerView rcProductos;
    FloatingActionButton flbtnNuevo;
    ContentResolver _resolver;
    ZXingScannerView mScannerView;
    AdapterProductos mAdapteProductos;
    private FirebaseAuth mAuth;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rcProductos = (RecyclerView) findViewById(R.id.rcProductos);
         flbtnNuevo =(FloatingActionButton) findViewById(R.id.flblNuevoProducto);
        _resolver = getContentResolver();


        rcProductos.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        rcProductos.setLayoutManager(layoutManager);

        rcProductos.setItemAnimator(new DefaultItemAnimator());

        flbtnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScannerView = new ZXingScannerView(MainActivity.this);
                setContentView(mScannerView);
                mScannerView.setResultHandler(MainActivity.this);
                mScannerView.setAutoFocus(true);
                mScannerView.startCamera();

            }
        });

        startService(new Intent(MainActivity.this, SyncFirebase.class));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(mAdapteProductos !=null)
                mAdapteProductos.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                sincronizaFirebase();
                return true;

                case R.id.sing_out:
                    cerrarSesion();
                    return true;
            
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void cerrarSesion() {
        try {
            FirebaseAuth.getInstance().signOut();
            LoginManager.getInstance().logOut();
            ProductosController productos = new ProductosController();
            productos.limpiarProductos();
            stopService(new Intent(MainActivity.this,SyncFirebase.class));

            finish();
        }
        catch (Exception e){

        }
    }

    private void sincronizaFirebase() {
        try {
            mProgressDialog = new ProgressDialog(MainActivity.this,
                    R.style.Productos_Theme_Dark_Dialog);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage("Sincronizando datos con el servidor....");
            mProgressDialog.show();
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            DatabaseReference usersRef = ref.child("productos");
            mAuth = FirebaseAuth.getInstance();
            FirebaseUser userFacebook = mAuth.getCurrentUser();
            Map<String, Producto> productos = new HashMap<>();
            Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.getURIProductosFilter("async"), null, null, null, null);
            DatabaseUtils.dumpCursor(_c);
            if(_c.getCount() > 0 ){
            _c.moveToPosition(0);
            do {
                productos.put(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)),
                        new Producto(
                                Integer.parseInt(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.ID_PRODUCTO))),
                                _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)),
                                _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO)),
                                _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO)),
                                Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.PRECIO_PRODUCTO))),
                                _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.URLIMG_PRODUCTO)),
                                _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO)),
                                Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LNG_PRODUCTO))),
                                Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LAT_PRODUCTO))),
                                userFacebook.getDisplayName(),
                                "1"));

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference();
                Uri file = Uri.fromFile(new File(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.URLIMG_PRODUCTO))));
                StorageReference riversRef = storageRef.child("images/" + _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)));
                UploadTask uploadTask = riversRef.putFile(file);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                        // ...
                    }
                });

            } while (_c.moveToNext());
            _c.close();

                usersRef.push().setValue(productos).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(@NonNull Void T) {

                    Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.getURIProductosFilter("async"), null, null, null, null);
                    DatabaseUtils.dumpCursor(_c);
                    _c.moveToPosition(0);
                    do {

                        ArrayList<ContentProviderOperation> _ops = new ArrayList<>();
                        _ops.add(ContentProviderOperation.newUpdate(ProductosBDContract.productos_articulos.getURIProductos(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.ID_PRODUCTO))))
                                .withValue(ProductosBDContract.productos_articulos.ASYNC_PRODUCTO, "1")
                                .build());
                        try {
                            _resolver.applyBatch(ProductosBDContract.AUTHORITY_PRODUCTOS, _ops);


                        } catch (RemoteException e) {
                            e.printStackTrace();

                        } catch (OperationApplicationException e) {
                            e.printStackTrace();
                        }


                    } while (_c.moveToNext());
                    _c.close();


                    if (mProgressDialog != null)
                        mProgressDialog.dismiss();


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //display error message
                    if (mProgressDialog != null)
                        mProgressDialog.dismiss();

                    Toast.makeText(MainActivity.this, R.string.error_sincronizar, Toast.LENGTH_LONG).show();
                }
            });
        }else{
                if (mProgressDialog != null)
                    mProgressDialog.dismiss();
                Toast.makeText(MainActivity.this, R.string.no_sincronizado, Toast.LENGTH_LONG).show();
            }

        }
        catch (Exception e){
            e.printStackTrace();
            if (mProgressDialog != null)
                mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AskForPermissions();
        ProductosBDInstance.getInstancia(this);
        listarProductos();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mScannerView !=null)
            mScannerView.stopCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    //Resultado del lector de código de barra
    @Override
    public void handleResult(Result rawResult) {

        try{

            mScannerView.resumeCameraPreview(this);
            setContentView(R.layout.activity_main);//Nos envia a la vista
            mScannerView.stopCamera();
            if(rawResult.getText() != "") {

                Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.getURIProductos(rawResult.getText()), null, null, null, null);
                if (_c.getCount() > 0){
                    Intent intent = new Intent(MainActivity.this, DetallesProducto.class);
                    intent.putExtra("codigo", rawResult.getText());
                    startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));

                }else {
                    Intent intent = new Intent(MainActivity.this, NuevoProducto.class);
                    intent.putExtra("codigo", rawResult.getText());
                    startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                }
            }else{
                Toast.makeText(MainActivity.this, R.string.error_codigo,
                        Toast.LENGTH_SHORT).show();
            }


        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_NUEVO_ARTICULO) {
            if (resultCode == RESULT_OK) {
            listarProductos();
            }
        }
    }

    private void listarProductos() {
        try{
            Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.URI_CONTENT, null, null, null, null);
            ArrayList<Producto> mArrayProducts = new ArrayList<>();

            _c.moveToPosition(0);
          do {
                mArrayProducts.add(
                            new Producto(
                                    Integer.parseInt(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.ID_PRODUCTO))),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.SERIE_PRODUCTO)),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO)),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO)),
                                    Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.PRECIO_PRODUCTO))),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.URLIMG_PRODUCTO)),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO)),
                                    Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LNG_PRODUCTO))),
                                    Double.parseDouble(_c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.LAT_PRODUCTO))),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.USUARIO_ALTA_PRODUCTO)),
                                    _c.getString(_c.getColumnIndex(ProductosBDContract.productos_articulos.ASYNC_PRODUCTO))));

            }while( _c.moveToNext());
            _c.close();
            if(mArrayProducts != null){
                 mAdapteProductos = new  AdapterProductos(mArrayProducts);
                rcProductos.setAdapter(mAdapteProductos);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    private List<Producto> getFakeDAta() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String fechaEv = df.format(c.getTime());

        List<Producto> objProducto = new ArrayList<>();
        /*for (int i = 0; i<10; i++){
            objProducto.add(new Producto(i,
                    "23",
                    "Coca Cola",
                    "Super buena",
                    14.00,
                    "",
                        c.getTime(),
                    "",
                    "",
                    "",
                    ""));
        }*/
        return objProducto;
    }


    public void AskForPermissions() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{CAMERA},
                    REQUEST_TAKE_PHOTO);
        }

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_TAKE_PHOTO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //
                } else {
                    AskForPermissions();
                }
                return;
            }

        }
    }

}
