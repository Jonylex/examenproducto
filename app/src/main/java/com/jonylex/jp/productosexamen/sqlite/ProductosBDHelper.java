package com.jonylex.jp.productosexamen.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.jonylex.jp.productosexamen.utils.Constantes;

public class ProductosBDHelper extends SQLiteOpenHelper {
    private static final String NOMBRE_BD = Constantes.DATABASE_NAME;
    private static final Integer VERSION = Constantes.DATABASE_VERSION;
    private final Context _my_context;

    //(1) - Nombre de las Tablas.
    public interface Tablas{
        String PRODUCTOS = "productos_articulos";

    }


    public ProductosBDHelper(Context _my_context){
        super(_my_context,NOMBRE_BD,null,VERSION);
        this._my_context = _my_context;
    }

    //Constructor sobrecargado.
    public ProductosBDHelper(Context _my_context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(_my_context,name,factory,version);
        this._my_context = _my_context;
    }


    @Override
    public void onOpen(SQLiteDatabase db){
        super.onOpen(db);
        if(!db.isReadOnly()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db){

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY ," + //id
                        "%s TEXT DEFAULT NULL," + //serie
                        "%s TEXT DEFAULT NULL," + //nombre
                        "%s TEXT DEFAULT NULL," + //descripcion
                        "%s NUMERIC DEFAULT NULL," + //precio
                        "%s TEXT DEFAULT NULL," + //url
                        "%s TEXT DEFAULT NULL," + //fecha
                        "%s TEXT DEFAULT NULL," + //fecha
                        "%s NUMERIC DEFAULT NULL," + //lon
                        "%s NUMERIC DEFAULT NULL," + //lat
                        "%s TEXT DEFAULT NULL  )", //sync
                Tablas.PRODUCTOS, ProductosBDContract.productos_articulos.ID_PRODUCTO,
                ProductosBDContract.productos_articulos.SERIE_PRODUCTO,
                ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO,
                ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO,
                ProductosBDContract.productos_articulos.PRECIO_PRODUCTO,
                ProductosBDContract.productos_articulos.URLIMG_PRODUCTO,
                ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO,
                ProductosBDContract.productos_articulos.USUARIO_ALTA_PRODUCTO,
                ProductosBDContract.productos_articulos.LNG_PRODUCTO,
                ProductosBDContract.productos_articulos.LAT_PRODUCTO,
                ProductosBDContract.productos_articulos.ASYNC_PRODUCTO
        ));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.PRODUCTOS);
        onCreate(db);
    }

}
