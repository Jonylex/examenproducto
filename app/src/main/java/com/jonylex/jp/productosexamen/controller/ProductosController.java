package com.jonylex.jp.productosexamen.controller;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.jonylex.jp.productosexamen.sqlite.ProductosBDContract;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDHelper;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDInstance;

public class ProductosController {

    /***
     * Obtiene la colección de todas los productos registradas.
     * @return Cursor.
     */
    public Cursor getProductos() {
        SQLiteDatabase db = ProductosBDInstance.objBD.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(ProductosBDHelper.Tablas.PRODUCTOS);

        String[] _resp = {
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ID_PRODUCTO ,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.SERIE_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.PRECIO_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.URLIMG_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LNG_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LAT_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.USUARIO_ALTA_PRODUCTO,
                ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ASYNC_PRODUCTO
        };

        return builder.query(db, _resp, null, null, null, null, ProductosBDContract.productos_articulos.PRECIO_PRODUCTO + " ASC");
    }

    /**
     * Obtiene la información de un usuario en especifico.
     * @param _ID // Identificador del producto.
     * @return
     */
    public Cursor getProductosBySerie(String _ID){
        try {

            SQLiteDatabase db = ProductosBDInstance.objBD.getReadableDatabase();
            String selection = String.format("%s=?", ProductosBDContract.productos_articulos.SERIE_PRODUCTO);
            String [] selectionArgs = {_ID};
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            builder.setTables(ProductosBDHelper.Tablas.PRODUCTOS);

            String[] _resp = {
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ID_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.SERIE_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.PRECIO_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.URLIMG_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LNG_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LAT_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.USUARIO_ALTA_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ASYNC_PRODUCTO

            };

            return builder.query(db, _resp, selection, selectionArgs, null, null, null);
        }
        catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }

    /**
     * Obtiene la información de un usuario en especifico.
     * @param _ID // Identificador del producto.
     * @return
     */
    public Cursor getProductosByNombre(String _ID){
        try {

            SQLiteDatabase db = ProductosBDInstance.objBD.getReadableDatabase();
            String selection = String.format("%s=?", ProductosBDContract.productos_articulos.SERIE_PRODUCTO);
            String [] selectionArgs = {_ID};
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            builder.setTables(ProductosBDHelper.Tablas.PRODUCTOS);

            String[] _resp = {
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ID_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.SERIE_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.PRECIO_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.URLIMG_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LNG_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LAT_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.USUARIO_ALTA_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ASYNC_PRODUCTO

            };

            return builder.query(db, _resp, selection, selectionArgs, null, null, null);
        }
        catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }


    /***
     * Obtiene la colección de todos los Productos por sincronizar con firebase.
     * @return
     */
    public Cursor getProductosToSync(String _syncFlag) {
        try {

            SQLiteDatabase db = ProductosBDInstance.objBD.getWritableDatabase();
            String selection = String.format("%s=?", ProductosBDContract.productos_articulos.ASYNC_PRODUCTO);
            String[] selectionArgs = {_syncFlag};

            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            builder.setTables(ProductosBDHelper.Tablas.PRODUCTOS);

            String[] _resp = {
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ID_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.SERIE_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.PRECIO_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.URLIMG_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LNG_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.LAT_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.USUARIO_ALTA_PRODUCTO,
                    ProductosBDHelper.Tablas.PRODUCTOS + "." + ProductosBDContract.productos_articulos.ASYNC_PRODUCTO

            };

            return builder.query(db, _resp, selection, selectionArgs, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /***
     * Añade un registro del tipo Producto a la BD Local.
     * @param _contentValues // Objeto del tipo content values.
     * @return // Identificador del cliente asignado. {Long}, (0 Cuando fallo la transacción.)
     */
    public long NuevoProducto(ContentValues _contentValues){
        try{
            SQLiteDatabase db = ProductosBDInstance.objBD.getWritableDatabase();
            return db.insertOrThrow(ProductosBDHelper.Tablas.PRODUCTOS,null,_contentValues);
        }
        catch (Exception e){
            e.printStackTrace();
            return  0;
        }
    }


    public int limpiarProductos(){
        try{
            SQLiteDatabase db = ProductosBDInstance.objBD.getWritableDatabase();
            return db.delete(ProductosBDHelper.Tablas.PRODUCTOS,null,null);
        }
        catch (Exception e){
            return 0;
        }

    }

    /**
     * Obtiene una instancia asociada de la BD.
     * @return
     */
    public SQLiteDatabase getDb() {
        return ProductosBDInstance.objBD.getWritableDatabase();
    }

    public int actualizarProductosBD(String _ID, ContentValues _contentValues) {
        try {
            SQLiteDatabase db = ProductosBDInstance.objBD.getWritableDatabase();
            String whereClause = String.format("%s=?", ProductosBDContract.productos_articulos.ID_PRODUCTO);
            String[] whereArgs = {_ID};

            return db.update(ProductosBDHelper.Tablas.PRODUCTOS, _contentValues, whereClause, whereArgs);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
