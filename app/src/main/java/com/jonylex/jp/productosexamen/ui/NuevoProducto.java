package com.jonylex.jp.productosexamen.ui;



import android.Manifest;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.Result;
import com.jonylex.jp.productosexamen.R;
import com.jonylex.jp.productosexamen.model.Producto;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDContract;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDInstance;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.jonylex.jp.productosexamen.utils.Tools.galleryAddPic;

public class NuevoProducto extends AppCompatActivity implements
         GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener{


    private static final String TAG =NuevoProducto.class.getName() ;
    LocationManager locationManager;
    Toolbar toolbar;
    TextView tvTomarFoto;
    ImageView ivImgProducto;
    Button btAddArticulo;
    EditText txtNombreProducto;
    EditText txtDescProducto;
    EditText txtPrecioProducto;
    TextView txtCodigoProducto ;

    String mCurrentPhotoPath = "";
    LocationRequest mLocationRequest;

    GoogleApiClient mGoogleApiClient;
    ContentResolver _resolver;
    Location mCurrentLocation;

    static final int REQUEST_TAKE_PHOTO = 1;
    static final int WRITE_PERMISSIONS = 2;
    static final int LOCATION_PERMISSIONS = 3;

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient != null)
            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();
        ProductosBDInstance.getInstancia(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        checkLocation();
        AskForPermissions();



    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_producto);
        ivImgProducto = (ImageView) findViewById(R.id.ivImgProducto);
        tvTomarFoto = (TextView) findViewById(R.id.tvTomarFoto);
        btAddArticulo = (Button) findViewById(R.id.btAddArticulo);
         txtNombreProducto = (EditText) findViewById(R.id.txtNombreProducto);
         txtDescProducto = (EditText) findViewById(R.id.txtDescProducto);
         txtPrecioProducto = (EditText) findViewById(R.id.txtPrecioProducto);
        txtCodigoProducto = (TextView) findViewById(R.id.txtCodigoProducto);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        _resolver = getContentResolver();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Agrega más productos");
        if(getIntent().getExtras() != null)
        txtCodigoProducto.setText(getIntent().getExtras().getString("codigo"));

        buildGoogleApiClient();
        tvTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        btAddArticulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validaDatos()){
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    final String fechaAlta = df.format(c.getTime());
                    Producto objProducto = new Producto(
                            0,
                            txtCodigoProducto.getText().toString(),
                            txtNombreProducto.getText().toString(),
                            txtDescProducto.getText().toString(),
                            Double.parseDouble(txtPrecioProducto.getText().toString()),
                            mCurrentPhotoPath,
                            fechaAlta,
                            mCurrentLocation.getLongitude(),
                            mCurrentLocation.getLatitude(),
                            "",
                            "0"
                    );
                    if(objProducto != null)
                        agregarProducto(objProducto);

                }else{
                    Toast.makeText(NuevoProducto.this, R.string.datos_invalidos, Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void agregarProducto(Producto objProducto) {
        try{

            ArrayList<ContentProviderOperation> _ops = new ArrayList<>();
            _ops.add(ContentProviderOperation.newInsert(ProductosBDContract.productos_articulos.URI_CONTENT)
                    .withValue(ProductosBDContract.productos_articulos.SERIE_PRODUCTO, objProducto.get_productos_serie())
                    .withValue(ProductosBDContract.productos_articulos.NOMBRE_PRODUCTO, objProducto.get_productos_nombre_producto())
                    .withValue(ProductosBDContract.productos_articulos.DESCRIPCION_PRODUCTO, objProducto.get_productos_descripción())
                    .withValue(ProductosBDContract.productos_articulos.PRECIO_PRODUCTO, objProducto.get_productos_precio())
                    .withValue(ProductosBDContract.productos_articulos.URLIMG_PRODUCTO, objProducto.get_productos_urlImg())
                    .withValue(ProductosBDContract.productos_articulos.FECHA_ALTA_PRODUCTO, objProducto.get_productos_fecha_alta())
                    .withValue(ProductosBDContract.productos_articulos.LNG_PRODUCTO, objProducto.get_productos_lng_producto())
                    .withValue(ProductosBDContract.productos_articulos.LAT_PRODUCTO, objProducto.get_productos_lat_producto())
                            .withValue(ProductosBDContract.productos_articulos.USUARIO_ALTA_PRODUCTO, objProducto.get_productos_usuario_alta())
                            .withValue(ProductosBDContract.productos_articulos.ASYNC_PRODUCTO, objProducto.get_productos_async())
                    .build());
            try {
                _resolver.applyBatch(ProductosBDContract.AUTHORITY_PRODUCTOS, _ops);
                Cursor _c = _resolver.query(ProductosBDContract.productos_articulos.URI_CONTENT, null, null, null, null);
                DatabaseUtils.dumpCursor(_c);

                Intent intent = new Intent();
                setResult(Activity.RESULT_OK, intent);
                finish();


            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }




    private boolean validaDatos(){
        return !txtNombreProducto.getText().toString().equals("") &&
                !txtDescProducto.getText().toString().equals("") &&
                !txtPrecioProducto.getText().toString().equals("") &&
                !txtCodigoProducto.getText().toString().equals("") &&
                mCurrentPhotoPath != "";
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Bitmap bitmap = galleryAddPic(mCurrentPhotoPath, ivImgProducto, NuevoProducto.this);

            if (bitmap != null)
                ivImgProducto.setImageBitmap(bitmap);
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }






    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ){//&& ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location l =LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (l != null) {
             mCurrentLocation = new Location(l);
            Log.i(TAG, "lat " + l.getLatitude());
            Log.i(TAG, "lng " + l.getLongitude());

            startLocationUpdate();
        }else{
            Toast toast = Toast.makeText(getBaseContext(), "Necesita activar su GPS ", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast toast = Toast.makeText(getBaseContext(), "Necesita activar su GPS Connection ", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mCurrentLocation = new Location(location);

        //startLocationUpdate();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void initLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(30000);
        mLocationRequest.setFastestInterval(30000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    private void startLocationUpdate() {
        initLocationRequest();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ){//&& ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }



    public void AskForPermissions() {
        if (ContextCompat.checkSelfPermission(NuevoProducto.this,
                CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(NuevoProducto.this,
                    new String[]{CAMERA},
                    REQUEST_TAKE_PHOTO);
        }


        if (ContextCompat.checkSelfPermission(NuevoProducto.this,
                ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(NuevoProducto.this,
                    new String[]{ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSIONS);
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            if(mGoogleApiClient != null)
                if (!mGoogleApiClient.isConnected())
                    mGoogleApiClient.connect();
            buildGoogleApiClient();



        }


        if (ContextCompat.checkSelfPermission(NuevoProducto.this,
                WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(NuevoProducto.this,
                    new String[]{WRITE_EXTERNAL_STORAGE},
                    WRITE_PERMISSIONS);
        }

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case LOCATION_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    buildGoogleApiClient();
                } else {
                    //AskForPermissions();
                }
                return;
            }

            case WRITE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //
                } else {
                    //AskForPermissions();
                }
                return;
            }

            case REQUEST_TAKE_PHOTO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //
                } else {
                    AskForPermissions();
                }
                return;
            }

        }
    }


    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                        "usa esta app")
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

}



