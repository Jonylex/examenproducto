package com.jonylex.jp.productosexamen.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.jonylex.jp.productosexamen.controller.ProductosController;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDContract;
import com.jonylex.jp.productosexamen.sqlite.ProductosBDHelper;
import com.jonylex.jp.productosexamen.utils.Constantes;

public class ProductosProvider  extends ContentProvider {
    /***
     * Instancia global del ContentResolver
     */
    private ContentResolver _resolver;

    /***
     * Instancia del administrador de la BD...
     */
    private ProductosBDHelper _databaseHelper;

    /**
     * Creamos los identificadores {match}.
     */
    public static final UriMatcher _uriMatcher;

    /***
     * Realizamos la instancia a los controllers.
     */
    private ProductosController objProductosController = new ProductosController();

    public static final int PRODUCTOS = 10;
    public static final int PRODUCTOS_ID = 11;
    //public static final int PRODUCTOS_NOMBRE = 202;

    static{
        _uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        _uriMatcher.addURI(ProductosBDContract.AUTHORITY_PRODUCTOS,"productos_articulos",PRODUCTOS);
        _uriMatcher.addURI(ProductosBDContract.AUTHORITY_PRODUCTOS,"productos_articulos/*",PRODUCTOS_ID);
      //  _uriMatcher.addURI(ProductosBDContract.AUTHORITY_PRODUCTOS,"productos_articulos/*",PRODUCTOS_NOMBRE);
    }

    /***
     * Constructor...
     */
    public ProductosProvider(){}


    @Override
    public boolean onCreate() {
        _databaseHelper = new ProductosBDHelper(getContext(),
                Constantes.DATABASE_NAME,null, Constantes.DATABASE_VERSION);
        _resolver = getContext().getContentResolver();
        return true;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.i("TAG",uri.toString());

        SQLiteDatabase _BD = _databaseHelper.getReadableDatabase();
        //Comparamos URI.
        Cursor c;
        int u = _uriMatcher.match(uri);


        switch(_uriMatcher.match(uri)) {
            case PRODUCTOS:
                String _filter = ProductosBDContract.productos_articulos.getFilter(uri) ? uri.getQueryParameter("filter") : null;

                if (_filter != null) {
                    if (_filter.equals("async")) {
                        c = objProductosController.getProductosToSync("0");
                    } else if (_filter.equals("sync")) {
                        c = objProductosController.getProductosToSync("1");
                    } else {
                        c = null;
                    }

                    break;
                }else{
                    c = objProductosController.getProductos();
                    break;
                }
            case PRODUCTOS_ID:
                String ID_PRODUCTO = ProductosBDContract.productos_articulos.getIDProducto(uri);
                c = objProductosController.getProductosBySerie(ID_PRODUCTO);
                break;
          /*  case PRODUCTOS_NOMBRE:
                String NOMBRE_PRODUCTO = ProductosBDContract.productos_articulos.getIDProducto(uri);
                c = objProductosController.getProductosBySerie(NOMBRE_PRODUCTO);
                break;*/
            default:
                throw new UnsupportedOperationException("Se desconoce la URI...");
        }
        if(c!=null){
            c.setNotificationUri(_resolver, uri);
        }
        return c;
    }


    @Override
    public String getType(Uri uri) {
        switch(_uriMatcher.match(uri)){
            case PRODUCTOS:
                return ProductosBDContract.getMimeDir("productos_articulos");
            case PRODUCTOS_ID:
                return ProductosBDContract.getMimeItem("productos_articulos");
            default:
                throw new UnsupportedOperationException("Se desconoce la URI...");
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        String _ID = null;

        switch(_uriMatcher.match(uri)){
            case PRODUCTOS:
                _ID = Long.toString(objProductosController.NuevoProducto(values));
                return ProductosBDContract.productos_articulos.getURIProductos(_ID);
            default:
                throw new UnsupportedOperationException("Se desconoce la URI...");
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase _database = _databaseHelper.getWritableDatabase();
        String _ID;
        int _rowsAffected = 0;

        switch(_uriMatcher.match(uri)){
            case PRODUCTOS_ID:
                _ID = ProductosBDContract.productos_articulos.getIDProducto(uri);
                _rowsAffected = objProductosController.actualizarProductosBD(_ID,values);
                _notifyChange(uri);
                break;
            default:
                throw new UnsupportedOperationException("Se desconoce la URI...");
        }
        return _rowsAffected;
    }



    private void _notifyChange(Uri uri){
        _resolver.notifyChange(uri,null);
    }
}

