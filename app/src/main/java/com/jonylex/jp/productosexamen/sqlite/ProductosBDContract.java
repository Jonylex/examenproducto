package com.jonylex.jp.productosexamen.sqlite;

import android.net.Uri;

import java.util.Date;

public class ProductosBDContract {

    //Definimos la autoridad del ContentProvider {Es un identificador único respecto a las demás aplicaciones que sincronizan cn un ContentProvider}.
    public static final String AUTHORITY_BASE = "com.jonylex.jp.productosexamen.base";
    public static final String AUTHORITY_PRODUCTOS = "com.jonylex.jp.productosexamen.productos";


    //Se utiliza la notación "Content://" para que el framework de Android, sepa que se refiere a una URI del ContentProvider
    public static final Uri URI_BASE = Uri.parse("content://" + AUTHORITY_BASE);
    public static final Uri URI_BASE_PRODUCTOS = Uri.parse("content://" + AUTHORITY_PRODUCTOS);


    //Definimos las Rutas, que emplearemos para las operaciones CRUD {Nombre de las tablas de la BD}.
    public static final String PRODUCTOS = "productos_articulos";


    //Definimos los tipos MIME.
    public static final String CONTENT_BASE = "productosexamen.";
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + CONTENT_BASE;
    public static final String CONTENT_TYPE_ITEM = "vnd.android.cursor.item/vnd." + CONTENT_BASE;

    //Contructores.
    private ProductosBDContract(){
    }

    //Genera un tipo de colecciones para recursos.
    public static String getMimeDir(String _ID){
        if(_ID != null){
            return CONTENT_TYPE + _ID;
        }
        return null;
    }

    //Genera un tipo para items {individuales}
    public static String getMimeItem(String _ID){
        if(_ID != null){
            return CONTENT_TYPE_ITEM + _ID;
        }
        return null;
    }

    //Utilizamos interfaces para crear cada una de las entidades relacionales.
    interface cols_productos_articulos{

        String ID_PRODUCTO =  "_productos_id_producto";
        String SERIE_PRODUCTO =  "_productos_serie";
        String NOMBRE_PRODUCTO =  "_productos_nombre_producto";
        String DESCRIPCION_PRODUCTO =  "_productos_descripción";
        String PRECIO_PRODUCTO =  "_productos_precio";
        String URLIMG_PRODUCTO =  "_productos_urlImg";
        String FECHA_ALTA_PRODUCTO =  "_productos_fecha_alta";
        String LNG_PRODUCTO =  "_productos_lng_producto";
        String LAT_PRODUCTO =  "_productos_lat_producto";
        String USUARIO_ALTA_PRODUCTO =  "_productos_usuario_alta";
        String ASYNC_PRODUCTO =  "_productos_async";

    }



    public static class productos_articulos implements cols_productos_articulos {
        //URIS
        public static final Uri URI_CONTENT =
                URI_BASE_PRODUCTOS.buildUpon().appendPath(PRODUCTOS).build();
        /***
         * Identificador de los filtros.
         */
        public static final String PARAMETRO_FILTRO = "filter";
        /**
         * Obtiene el ID, enviado en la URI.
         * @param uri, Ruta del provider.
         * @return ID.
         */
        public static String getIDProducto(Uri uri){return uri.getPathSegments().get(1);}
        /**
         * Añade un filtro a la consulta.
         * @param _FILTER
         * @return
         */
        public static Uri getURIProductosFilter(String _FILTER){
            return URI_BASE_PRODUCTOS.buildUpon()
                    .appendPath(PRODUCTOS)
                    .appendQueryParameter("filter", _FILTER).build();
        }

        /***
         * Obtiene el URI, de la entidad insertada.
         * @param _ID
         * @return
         */
        public static Uri getURIProductos(String _ID){return URI_CONTENT.buildUpon().appendPath(_ID).build();}
        /***
         * Obtiene el filtro.
         * @param uri
         * @return
         */
        public static boolean getFilter(Uri uri){return uri != null && uri.getQueryParameter(PARAMETRO_FILTRO) != null;}

        public String[] getPROJECTION(){
            final String [] PROUCTOS_ARTICULOS_PROJECTION = new String[]{

             productos_articulos.ID_PRODUCTO,
            productos_articulos.SERIE_PRODUCTO,
            productos_articulos .NOMBRE_PRODUCTO,
            productos_articulos .DESCRIPCION_PRODUCTO,
            productos_articulos .PRECIO_PRODUCTO,
            productos_articulos .URLIMG_PRODUCTO ,
            productos_articulos. FECHA_ALTA_PRODUCTO ,
            productos_articulos. LNG_PRODUCTO ,
            productos_articulos. LAT_PRODUCTO ,
            productos_articulos. USUARIO_ALTA_PRODUCTO ,
            productos_articulos. ASYNC_PRODUCTO ,
            };

            return PROUCTOS_ARTICULOS_PROJECTION;
        }
    }




}

